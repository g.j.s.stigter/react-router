import React from 'react';
import './App.css';
import {NavLink, Link} from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Nav extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menuOpen: false,
        };
    }

    // componentDidMount = () => {
    //     const currentState = this.state.menuOpen;
    //     this.setState({menuOpen: !currentState});
    // };

    toggleMenu = () => {
        const currentState = this.state.menuOpen;
        this.setState({menuOpen: !currentState});
    };

    render() {
        return (
            <div className="Nav">
                <nav>
                    <h1><Link to='/'>Oceanshade</Link></h1>
                    <p onClick={this.toggleMenu}><FontAwesomeIcon icon="bars"/></p>
                </nav>
                <div className={'Nav-menu ' + (this.state.menuOpen ? 'active' : '')}>
                    <ul>
                        <li><NavLink to={{ pathname: '/'}} exact onClick={this.toggleMenu}>Home</NavLink></li>
                        <li><NavLink to='/projects' onClick={this.toggleMenu}>Projects</NavLink></li>
                        <li><NavLink to='/about' onClick={this.toggleMenu}>About</NavLink></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Nav;
