import React from 'react';
import './App.css';

class Project extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            project: [],
            isLoading: false,
        };
    }

    componentDidMount(match) {
        this.setState({ isLoading: true });
        fetch('https://gitlab.com/api/v4/projects/' + this.state.id,
            // {
            // method: 'POST',
            // headers: {
            //     'Authorization': 'Bearer x3VUCfor9q4SVxwxWo4W',
            //     'Content-Type': 'application/x-www-form-urlencoded',
            // },
            // }
        )
            .then(response => response.json())
            .then(data => this.setState({ project: data, isLoading: false }));
    }

    render() {
        const { project, isLoading } = this.state;
        if (isLoading) {
            return(
                <main className="Projects">
                    <h2>Project: {this.state.id}</h2>
                    <p>Loading ...</p>
                </main>
            );
        }
        return (
            <main className="Projects">
                <h2>{project.name} <span> {new Date(project.last_activity_at).toDateString()}</span></h2>
                <div>{project.star_count}</div>
                <pre>
                {JSON.stringify(project, null, 4)}
                </pre>
            </main>
        );
    }
}

export default Project;
