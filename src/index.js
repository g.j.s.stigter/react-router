import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

/**
 * Styling
 */
import './main.scss';

/**
 * FontAwesome
 */
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {faBars, faCheckSquare, faCoffee} from '@fortawesome/free-solid-svg-icons';

/**
 * Pages
 */
import Nav from './Nav'
import E404 from "./404";
import Footer from "./Footer";
import Projects from "./Projects";
import Project from "./Project";

library.add(fab, faCheckSquare, faCoffee, faBars);

const routing = (
    <Router>
        <div>
            <Nav/>
            <Switch>
                <Route path="/" exact component={App}/>
                <Route path="/projects" exact component={Projects}/>
                <Route path="/projects/:id" exact component={Project}/>
                <Route component={E404} />
            </Switch>
            <Footer/>
        </div>
    </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
