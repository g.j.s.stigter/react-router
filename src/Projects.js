import React from 'react';
import './App.css';
// import {Link} from "react-router-dom";

class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        fetch('https://gitlab.com/api/v4/users/4589462/projects',
        // {
        //     method: 'POST',
        //     headers: {
        //         'Authorization': 'Bearer x3VUCfor9q4SVxwxWo4W',
        //         'Content-Type': 'application/x-www-form-urlencoded',
        //     },
        // }
        )
        .then(response => response.json())
        .then(data => this.setState({ projects: data, isLoading: false }));
    }

    render() {
        const { projects, isLoading } = this.state;
        if (isLoading) {
            return(
                <main className="Projects">
                    <h2>Projects</h2>
                    <p>Loading ...</p>
                </main>
            );
        }
        return (
            <main className="Projects">
                <h2>Projects</h2>
                <ul>
                    {JSON.stringify(this.state.projects, ' ', '\n')}
                    {projects.map(projects =>
                        <li key={projects.id}>
                            <a href={'projects/' + projects.id}>Joe</a>
                        </li>
                    )}
                </ul>
            </main>
        );
    }
}

export default Projects;
