import React from 'react';
import './App.css';
// import {Link} from "react-router-dom";

class Footer extends React.Component {
    render() {
        return (
            <footer className="Footer">
                <h2>Contact</h2>
                <dl>
                    <dt>Phone:</dt>
                    <dd><a href='tel:+31655158588'>+31655158588</a></dd>
                </dl>
            </footer>
        );
    }
}

export default Footer;
