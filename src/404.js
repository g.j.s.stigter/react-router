import React from 'react';
import './App.css';
import {Link} from "react-router-dom";

class E404 extends React.Component {
    render() {
        return (
            <main className="E404">
                <h1>404 Page not found.</h1>
                <Link to='/'>Go back to home</Link>
            </main>
        );
    }
}

export default E404;
